/*
  Ejercicio: Realización de un programa en jQuery
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: Programa que lee un archivo csv, crea una estructura JSON con los datos del archivo, la presenta en una etiqueta HTML específica y permite ordenar la estructura de forma ascendente y descendente
*/
$(document).ready(function(){
	// Leemos archivo csv
	leerArchivoCSV();
});
function leerArchivoCSV(){
	// Leemos archivo csv
	var archivo = new XMLHttpRequest();
	archivo.open('GET', 'csv.txt', false);
	archivo.onreadystatechange = function(){
		// Verificamos archivo csv
		if(archivo.readyState === 4 && (archivo.status === 200 || archivo.status == 0)){
			var csv = archivo.responseText;
			// Creamos estructura JSON con datos de archivo csv
			json = crearJSON(csv);
			// Presentamos estructura JSON
			presentarJSON(json);
			// Guardamos estructura JSON en variable global
			$json = json;
			// Guardamos orden en variable global
			$orden = 'asc';
		}
	}
	archivo.send(null);
}
function crearJSON(csv){
	// Creamos estructura JSON con datos de archivo csv 
	var json = [];
	var lineas = csv.split('\n');
	var titulos = lineas[0].split(",");
	// Obtenemos datos de archivo csv
	for(i in lineas){
		if(i > 0){
			// Creamos elemento para estructura JSON
			var elemento = {};
			var linea = lineas[i].split(",");
			for(j in titulos)
				elemento[titulos[j]] = linea[j];
			// Agregamos elemento a estructura JSON
			json.push(elemento);
		}
	}
	return json;
}
function presentarJSON(json){
	// Presentamos estructura JSON
	$('#json').html('<thead><tr></tr></thead><tbody></tbody>');
	// Presentamos titulares
	for(i in json[0])
		$('#json thead tr').append('<th><a href="#">' + i + '</a></th>');
	// Ocultamos contenido
	$('#json tbody').fadeOut(0);
	// Presentamos contenido
	for(i in json){
		linea = '';
		for(j in json[0])
			linea += '<th>' + json[i][j] + '</th>';
		$('#json tbody').append('<tr>' + linea + '</tr>');
	}
	// Presentamos contenido
	$('#json tbody').fadeIn('slow');
	// Evento para ordenar estructura JSON
	$('#json thead tr a').click(function(){
		ordenarJSON($json, $(this).html(), $orden);
		// Guardamos orden en variable global
		if($orden == 'asc')
			$orden = 'dsc';
		else
			$orden = 'asc';
	});
}
function ordenarJSON(json, ordenar, orden){
	// Ordenamos estructura JSON
	json = json.sort(function(a, b){
		var x = a[ordenar]; 
		var y = b[ordenar];
        if(orden == 'dsc')
        	return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        else
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
	// Presentamos estructura JSON
	presentarJSON(json);
}